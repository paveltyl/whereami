//
//  SecondViewController.swift
//  WhereAmI
//
//  Created by Pavel Tyletsky on 25/5/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController,
    UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    struct Frined {
        let name: String
        let id: Int
    }
    
    static var friends : [Frined] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.addSubview(self.refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return SecondViewController.friends.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "friendCell", for: indexPath)
        cell.textLabel?.text = SecondViewController.friends[(indexPath as NSIndexPath).row].name
        cell.detailTextLabel?.text = String(SecondViewController.friends[(indexPath as NSIndexPath).row].id)
        
        return cell
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(SecondViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        return refreshControl
    }()
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        //TODO: Send request and fill table with correct friendslist from VK. Don't forget about asyncronus request
        //let friends = Server.getFriendsList()
        
//        let newMovie = Frined(name: "Alex", id: 3)
//        SecondViewController.friends.append(newMovie)
        
        SecondViewController.friends.sort() { $0.name < $1.name }
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
}

