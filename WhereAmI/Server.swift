//
//  Server.swift
//  WhereAmI
//
//  Created by Pavel Tyletsky on 25/5/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit


class Server: NSObject {    
    static var currentName : String = ""
    static var currentId : Int = 0
    
    static let host : String = "alextur.me"
    static let port : Int = 3000
    
    static func generateId(_ name: String) -> Int {
        let rand = Int(arc4random_uniform(UInt32(UInt32.max)));
        let length = name.characters.count;
        
        var res = rand;
        
        for i in 0 ... length * 10 {
            let m = i % length;
            let aChar:unichar = (name as NSString).character(at: m)
            res -= Int(aChar) * ( m - i);
        }
        
        return res;
    }
    
    static func delete(){
        let url = URL(string: "http://\(Server.host):\(Server.port)/delete?id=\(Server.currentId)&name=\(Server.currentName)&latitude=0&longitude=0");
        
        let request: URLRequest = URLRequest(url: url!)
        do{
            let resp = try NSURLConnection.sendSynchronousRequest(request, returning: nil);
            print(resp)
        }catch let error as NSError{
            print(error);
        }
    }
    
    static func update(Coordinates coords: Array<String>) -> Dictionary<Int, User.Friend> {
        //TODO: Send your location and handle response
        
        let url = URL(string: "http://\(Server.host):\(Server.port)/update?id=\(Server.currentId)&name=\(Server.currentName)&latitude=\(coords[0])&longitude=\(coords[1])");
        
        var friends : Dictionary<Int, User.Friend> = [:];
        
        let request1: URLRequest = URLRequest(url: url!)
        
        do{
        let dataVal = try NSURLConnection.sendSynchronousRequest(request1, returning: nil);
            do {
                let jsonObject = try JSONSerialization.jsonObject(with: dataVal);
                
                print(jsonObject);
                
                if let array = jsonObject as? [Any]{
                    for object in array{
                        if let dictionary = object as? [String: Any] {
                            if let id = dictionary["id"] as? Int {
                                if let name = dictionary["name"] as? String{
                                    if let latitude = dictionary["latitude"] as? Double{
                                        if let longitude = dictionary["longitude"] as? Double{
                                            friends[id] = User.Friend(Name: name,
                                                                      Latitude: latitude,Longitude: longitude);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                
            } catch let error as NSError {
                print(error)
            }
        }catch let error as NSError{
            print(error);
        }
        
        return friends
    }
    
    static func getFriendsList(){
        
    }
}
