//
//  FirstViewController.swift
//  WhereAmI
//
//  Created by Pavel Tyletsky on 25/5/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit
import MapKit

let UpdatePeriod = 6

class FirstViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        startTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        FirstViewController.stopTimer()
        Server.delete()
        super.viewWillDisappear(animated);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func unwindToMenu(segue: UIStoryboardSegue) {
//        self.stopTimer()
    }
    
    static var timer: DispatchSourceTimer?
    
    func startTimer() {
        let lm = LocationManager()
        lm.startLocating()
        let queue = DispatchQueue(label: "com.domain.app.timer")
        FirstViewController.timer = DispatchSource.makeTimerSource(queue: queue)
        FirstViewController.timer!.setEventHandler { [weak self, lm] in
            let coords = lm.getLocations() as! Array<String>
            //TODO: replace paul with name
            let friends = Server.update(Coordinates: coords)
            self?.mapView.removeAnnotations((self?.mapView.annotations)!);
            SecondViewController.friends.removeAll()
            for friend in friends.values{
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2DMake(friend.lat, friend.lon)
                annotation.title = friend.name
                self?.mapView.addAnnotation(annotation)
                SecondViewController.friends.append(SecondViewController.Frined(name: friend.name, id: 0))
//                self?.mapView.update
                //TODO: Create annotations redrawing
            }
        }
        FirstViewController.timer!.scheduleRepeating(deadline: .now(), interval: .seconds(UpdatePeriod))
        FirstViewController.timer!.resume()
    }
    
    static func stopTimer() {
        if FirstViewController.timer != nil{
            FirstViewController.timer?.cancel()
            FirstViewController.timer = nil
        }
    }
    
    deinit {
        print("deinit triggered");
    }
}

