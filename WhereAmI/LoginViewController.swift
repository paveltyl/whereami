//
//  FirstViewController.swift
//  WhereAmI
//
//  Created by Pavel Tyletsky on 25/5/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit
import MapKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var goButton: UIButton?
    @IBOutlet weak var textInput: UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func goButtonClicked(){
        Server.currentName = (textInput?.text)!
        Server.currentId = Server.generateId(Server.currentName)
        
        self.performSegue(withIdentifier: "GoToMap", sender: self);
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }

}

