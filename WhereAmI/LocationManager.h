//
//  LocationManager.h
//  Weather Forecast
//
//  Created by Pavel Tyletsky on 22/4/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : UITableViewController<CLLocationManagerDelegate>

@property (nonatomic, retain) CLLocationManager *locationManager;

- (void)startLocating;
- (NSArray*)getLocations;

@end
