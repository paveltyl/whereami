//
//  User.swift
//  WhereAmI
//
//  Created by Pavel Tyletsky on 25/5/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit

class User: NSObject {
    
    private struct defaultsKeys {
        static let id = "id"
    }
    
    static let shared = User()
    
    var id: Int
    
    private override init() {
        let defaults = UserDefaults.standard
        id = defaults.integer(forKey: defaultsKeys.id)
    }
    
    struct Friend {
        let lat : CLLocationDegrees
        let lon : CLLocationDegrees
        let name : String
        
        init(Name _name : String, Latitude _lat : CLLocationDegrees, Longitude _lon : CLLocationDegrees) {
            name = _name
            lat = _lat
            lon = _lon
        }
    }
}
